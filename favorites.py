first_question = "What is your favorite color?"
first_question_answer = "Yellow"

second_question = "What is your favorite food?"
second_question_answer = "Mexican"

third_question = "Who is your favorite fictional character? ex. Mickey Mouse, Bugs Bunny"
third_question_answer = "Aang"

fourth_question = "What is your favorite animal?"
fourth_question_answer = "Dog"

fifth_question = "What is your favorite programming language?"
fifth_question_answer = "Python"
